# Update domain certificate va Kong API

```zsh
#!/usr/bin/zsh

cert_dir=$1
snis=$2

curl -i -X POST http://localhost:8001/certificates \
    -F "cert=@${cert_dir}.pem" \
    -F "key=@${cert_dir}.key" \
    -F "snis=${snis}"

```